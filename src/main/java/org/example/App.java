package org.example;

import org.example.model.gitLabTraining;

import java.util.UUID;

/**
 * Hello world!
 *
 */
public class App 
{
    public static gitLabTraining getGitLabTraining(){
        gitLabTraining gitDetails = gitLabTraining.builder().build();
        gitDetails.setGetGitLabUserEmail("mrumanath@gmail.com");
        gitDetails.setGitLabUserName(UUID.randomUUID().toString());
        return gitDetails;
    }
    public static void main( String[] args )
    {
        System.out.println(getGitLabTraining().getGetGitLabUserEmail());
        System.out.println(getGitLabTraining().getGitLabUserName());
    }
}
